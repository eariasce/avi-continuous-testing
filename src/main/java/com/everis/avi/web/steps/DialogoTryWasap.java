package com.everis.avi.web.steps;

import com.everis.avi.objetos.ejecucion;
import com.everis.avi.rest.fact.Access;
import com.everis.avi.util.Conexion;
import com.everis.avi.util.FuncionesTryWs;
import com.everis.avi.web.fact.AccessAviTry;
import com.everis.avi.web.page.LoginPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.DefaultUrl;
import org.junit.Assert;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class DialogoTryWasap {
    int iCanidadEjecucion = 0;
    List<ejecucion> Ejecucion = new ArrayList<>();

    @Given("^The (.*) actor obtains the configuration data for the execution of the dictionaries$")
    public void theSantiagoActorObtainsTheConfigurationDataForTheExecutionOfTheDictionaries(String Actorname) throws Throwable {
        theActorCalled(Actorname).has(AccessAviTry.toAviTry());
        theActorInTheSpotlight().attemptsTo(Open.url("https://us-south.assistant.watson.cloud.ibm.com/us-south/crn:v1:bluemix:public:conversation:us-south:a~2F6dc7cbaf80a6c084c84021c777f1d3cc:a5f4a381-ef68-4076-8ad2-049d5c5a6034::/skills"));

        Ejecucion = new Conexion().ObtenerDatosCofiguracion();
        iCanidadEjecucion = Ejecucion.size();
        //Assert.assertEquals("No se encontraron ejecuciones programadas",iCanidadEjecucion>1,iCanidadEjecucion);
    }

    @When("^Enter watson try and perform the conversion to get the response codes$")
    public void enterWatsonTryAndPerformTheConversionToGetTheResponseCodes() throws SQLException, InterruptedException {
        new FuncionesTryWs().ObtenerDatosLogin();
    }

    @Then("^Save the dialogs and eliminate the temporary work area$")
    public void saveTheDialogsAndEliminateTheTemporaryWorkArea() {
        new Conexion().EliminarDatosTemporal();
        new Conexion().ReiniciarCampoIdentity();
    }
}