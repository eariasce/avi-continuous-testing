package com.everis.avi;

import com.everis.avi.objetos.ejecucion;
import com.everis.avi.util.Conexion;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.restassured.RestAssured;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.Cast;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.annotations.ClearCookiesPolicy;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Setup {

    int iCodigoChatBot = 0;
    String sIndidadorWS = "";
    String sAmbiente = "";
    List<ejecucion> Ejecucion = new ArrayList<>();


    @Managed(driver = "chrome")
    //PARA GUARDAR COOKIES Y MANTENER SESSION EN WHASTSAPP WEB
    //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     hn@Managed(uniqueSession = true, clearCookies= ClearCookiesPolicy.Never)
    WebDriver driver;

    @Before
    public void setTheStage() throws SQLException {

        Ejecucion = new Conexion().ObtenerDatosCofiguracion();
        for (ejecucion element : Ejecucion) {
            iCodigoChatBot = element.getiCodigoChatBot();
            sIndidadorWS = element.getsIndicadorWs();
            sAmbiente = element.getcAmbiente();
        }
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

        if (sIndidadorWS.toString().trim().equalsIgnoreCase("SI") && iCodigoChatBot == 5) {
            if (sAmbiente.equalsIgnoreCase("DEV")) {
                Cast cast = OnlineCast.whereEveryoneCan(CallAnApi.at("https://eu2-ibk-apm-dev-ext-001.azure-api.net/eva/conversations/"),
                        BrowseTheWeb.with(driver));
                OnStage.setTheStage(cast);
            } else if (sAmbiente.equalsIgnoreCase("STG")) {
                Cast cast = OnlineCast.whereEveryoneCan(CallAnApi.at("https://eu2-ibk-apm-stg-ext-001.azure-api.net/evabroker/conversations/"),
                        BrowseTheWeb.with(driver));
                OnStage.setTheStage(cast);}

            else{
               Cast cast = OnlineCast.whereEveryoneCan(CallAnApi.at("https://apis.uat.interbank.pe/evabroker/conversations/"),
                        BrowseTheWeb.with(driver));
                OnStage.setTheStage(cast);
            }

        } else if (sIndidadorWS.toString().trim().equalsIgnoreCase("SI") && iCodigoChatBot == 3) {
            if (sAmbiente.equalsIgnoreCase("DEV")) {
                Cast cast = OnlineCast.whereEveryoneCan(CallAnApi.at("https://us-south.assistant.watson.cloud.ibm.com/us-south/crn:v1:bluemix:public:conversation:us-south:a~2F6dc7cbaf80a6c084c84021c777f1d3cc:a5f4a381-ef68-4076-8ad2-049d5c5a6034::/skills"),
                        BrowseTheWeb.with(driver));
                OnStage.setTheStage(cast);
            } else {
                Cast cast = OnlineCast.whereEveryoneCan(CallAnApi.at("https://us-south.assistant.watson.cloud.ibm.com/us-south/crn:v1:bluemix:public:conversation:us-south:a~2F6dc7cbaf80a6c084c84021c777f1d3cc:3fdb0862-9d5d-4592-978e-218662b22313::/skills"),
                        BrowseTheWeb.with(driver));
                OnStage.setTheStage(cast);
            }

        } else if (sIndidadorWS.toString().trim().equalsIgnoreCase("NO") && iCodigoChatBot == 1) {
            if (sAmbiente.equalsIgnoreCase("DEV")) {
                Cast cast = OnlineCast.whereEveryoneCan(CallAnApi.at("https://eu2-ibk-apm-dev-ext-001.azure-api.net/eva/conversations/"),
                        BrowseTheWeb.with(driver));
                OnStage.setTheStage(cast);
            } else if (sAmbiente.equalsIgnoreCase("STG")) {
                Cast cast = OnlineCast.whereEveryoneCan(CallAnApi.at("https://eu2-ibk-apm-stg-ext-001.azure-api.net/evabroker/conversations/"),
                        BrowseTheWeb.with(driver));
                OnStage.setTheStage(cast);}

            else{
                Cast cast = OnlineCast.whereEveryoneCan(CallAnApi.at("https://apis.uat.interbank.pe/evabroker/conversations/"),
                        BrowseTheWeb.with(driver));
                OnStage.setTheStage(cast);
            }
        }
    }

    @After
    public void closeTheStage() {
        OnStage.drawTheCurtain();
    }
}