@ServicioENV
Feature: Conduct a dialogue with the avi service

  Scenario: Run the dialogue service and verify that it responds correctly
    When The client starts the conversation through the services
    And Get the first answer you will get the session and continue with the dictionary flow on env
    Then the response obtained with the dictionary will be verified
    And clean the database
